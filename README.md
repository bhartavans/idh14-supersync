Opdracht C. Eindopdracht
Ontwerp en bouw een client-remote-applicatie om bestanden tussen een aantal werkplekken via een centrale remote te synchroniseren.

De opdracht bestaat uit twee delen. Je beschrijft eerst met de hele klas de globale requirements en een applicatieprotocol. Als die goedgekeurd zijn, gaat elk groepje (van maximaal 4 personen) de details uitwerken en de client en remote bouwen.

Opdracht C1. Bepaal de requirements en ontwerp een gezamenlijk applicatieprotocol
Maak een lijst met requirements. Maak aan de hand daarvan een voorstel voor een applicatieprotocol waarbij je de volgende punten in acht neemt:

Zorg voor een correcte afhandeling van netwerkproblemen 
(storingen moeten geen verminkte of onvolledige bestanden achterlaten).
Zorg dat bestanden groter dan 4GB uitgewisseld kunnen worden.
Houd rekening met verschillende bestandsystemen bij bestandsnamen. 
(lengte, tekenset (UTF8 / UCS2), toegestane tekens, ...)
Op de remote moeten verschillende clients tegelijk aangesloten kunnen zijn.
Gebruik HTTP als inspiratie.
Opdracht C2. Ontwerp en bouw client en remote
Implementeer hierin je eerder beschreven applicatieprotocol.
Gebruik sockets (zie bijvoorbeeld het knock-knock-protocol bij Oracle)
Je hoeft niet per se een user-interface te bouwen.
Zorg voor een werkende demo, waaarin je de requirements aantoont.
Laat de client en de remote op verschillende fysieke machines draaien.