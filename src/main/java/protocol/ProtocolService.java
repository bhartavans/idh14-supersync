package protocol;

import protocol.models.Header;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

import static java.lang.Long.parseLong;

public class ProtocolService {
    public Header decodeHeader(ArrayList<String> headers) {
        Header header = new Header();

        int index = 0;
        for (String headerLine: headers) {
            if (index == 0) {
                // first header line
                String[] splittedLines0 = headerLine.split(" ");
                String[] splittedLines1 = Arrays.copyOfRange(splittedLines0, 1, splittedLines0.length);
                String[] splittedLines2 = splittedLines0[0].split("/");
                // protocol name and protocol versions
                header.protocolName = splittedLines2[0];
                header.protocolVersion = splittedLines2[1];
                // response code, response name and method
                for (String splittedLine: splittedLines1) {
                    if (splittedLine.matches("-?(0|[1-9]\\d*)")) {
                        header.responseCode = Integer.parseInt(splittedLine);
                    } else if (header.responseCode != null) {
                        if (header.responseName != null) {
                            header.responseName += " " + splittedLine;
                        } else {
                            header.responseName = splittedLine;
                        }
                    } else {
                        header.method = splittedLine;
                    }
                }
            } else {
                // header properties
                String[] splittedLine = headerLine.split(": ");
                String property = splittedLine[0];
                String value = splittedLine[1];
                if (Objects.equals(property, Config.Header.Properties.CONTENT_NAME)) {
                    header.contentName = value;
                } else if (Objects.equals(property, Config.Header.Properties.CONTENT_SIZE)) {
                    header.contentSize = parseLong(value);
                } else if (Objects.equals(property, Config.Header.Properties.CONTENT_CHECKSUM)) {
                    header.contentCheckSum = value;
                } else if (Objects.equals(property, Config.Header.Properties.CONTENT_ORIGINAL_CHECKSUM)) {
                    header.contentOriginalChecksum = value;
                }
            }
            index++;
        }


        System.out.println();
        System.out.println("INPUT HEADER VALUES");
        System.out.println("Protocol Name: " + header.protocolName);
        System.out.println("Protocol Version: " + header.protocolVersion);
        System.out.println("Protocol Method: " + header.method);
        System.out.println("Response Code: " + header.responseCode);
        System.out.println("Response Name: " + header.responseName);
        System.out.println("Content-name: " + header.contentName);
        System.out.println("Content-size: " + header.contentSize);
        System.out.println("Content-checksum: " + header.contentCheckSum);
        System.out.println("Content-original-checksum: " + header.contentOriginalChecksum);
        System.out.println();

        return header;
    }

    public byte[] encodeHeader(Header decodedHeader) {
        return decodedHeader.toByteArray();
    }
}
