package protocol.models;

import protocol.Config;

public class Header {
    public String protocolName = null;
    public String protocolVersion = null;
    public String method = null;
    public String contentName = null;
    public Long contentSize = null;
    public String contentCheckSum = null;
    public String contentOriginalChecksum = null;
    public Integer responseCode = null;
    public String responseName = null;

    public byte[] toByteArray() {
        String header = protocolName + "/" + protocolVersion;
        if (method != null) {
            header += " " + method + "\n";
        }
        if (responseCode != null && responseName != null) {
            header += " " + responseCode + " " + responseName + "\n";
        }
        if (contentName != null) {
            header += Config.Header.Properties.CONTENT_NAME + ": " + contentName + "\n";
        }
        if (contentSize != null) {
            header += Config.Header.Properties.CONTENT_SIZE + ": " + contentSize + "\n";
        }
        if (contentCheckSum != null) {
            header += Config.Header.Properties.CONTENT_CHECKSUM + ": " + contentCheckSum + "\n";
        }
        if (contentOriginalChecksum != null) {
            header += Config.Header.Properties.CONTENT_ORIGINAL_CHECKSUM + ": " + contentOriginalChecksum + "\n";
        }
        header += "\n";
        return header.getBytes();
    }
}
