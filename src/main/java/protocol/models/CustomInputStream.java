package protocol.models;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;

public class CustomInputStream extends BufferedInputStream {
    public CustomInputStream(InputStream in) {
        super(in);
    }

    public CustomInputStream(InputStream in, int size) {
        super(in, size);
    }

    public String readLine() throws IOException {
        StringBuilder builder = new StringBuilder();
        byte[] b = new byte[1];

        while (read(b) != -1) {
            if (b[0] == '\n') { // new line
                return builder.toString();
            } else {
                builder.append((char) b[0]);
            }
        }

        if (builder.length() == 0) {
            return null;
        } else {
            return builder.toString();
        }
    }
}
