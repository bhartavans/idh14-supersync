package protocol.models;

public class Response {
    private int code;
    private String name;

    public Response(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getCode() {
        return code;
    }
}
