package protocol.models;

import com.google.gson.annotations.SerializedName;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileEntry {
    @SerializedName("file-name")
    public String fileName;
    @SerializedName("content-size")
    public Long contentSize;
    public String checksum;
    public String originalCheckSum;

    public FileEntry(java.io.File file) {
        fileName = file.getName();
        contentSize = file.length();
        checksum = getChecksum(file);
    }

    /**
     * Get the checksum of a file
     * @param file
     * @return
     */
    private String getChecksum(java.io.File file) {
        byte[] bytes;
        String checksum = null;
        try {
            bytes = Files.readAllBytes(Paths.get(file.getPath()));
            byte[] hash = MessageDigest.getInstance("MD5").digest(bytes);
            checksum = DatatypeConverter.printHexBinary(hash);
        } catch (NoSuchAlgorithmException | IOException e) {
            e.printStackTrace();
        }

        return checksum;
    }
}
