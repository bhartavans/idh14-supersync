package protocol;

import protocol.models.Response;

/**
 * Created by Bram de Hart.
 */
public class Config {
    public static class Remote {
        public static final String HOST = "127.0.0.1";
        public static final int PORT = 2000;
    }

    public static class Header {
        public static final String PROTOCOL_NAME = "SYNC";
        public static final String PROTOCOL_VERSION = "1.0";

        public static class Methods {
            public static final String LIST = "LIST";
            public static final String DOWNLOAD = "DOWNLOAD";
            public static final String UPLOAD = "UPLOAD";
            public static final String DELETE = "DELETE";
        }

        public static class Properties {
            public static final String CONTENT_NAME = "Content-name";
            public static final String CONTENT_SIZE = "Content-size";
            public static final String CONTENT_ORIGINAL_CHECKSUM = "Content-original-checksum";
            public static final String CONTENT_CHECKSUM = "Content-checksum";
        }

        public static class Responses {
            public static final Response OK = new Response(200, "OK");
            public static final Response BAD_REQUEST = new Response(400, "BAD REQUEST");
            public static final Response NOT_FOUND = new Response(404, "NOT FOUND");
            public static final Response FILE_CONFLICT = new Response(409, "FILE CONFLICT");
            public static final Response FILE_SIZE_EXCEEDS = new Response(213, "FILE SIZE EXCEEDS");
            public static final Response CHECKSUM_MISMATCH = new Response(419, "CHECKSUM MISMATCH");
            public static final Response SERVER_ERROR = new Response(500, "SERVER ERROR");
        }
    }

}
