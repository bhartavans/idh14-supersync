package client;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by Bram de Hart.
 */
public class Main {

    public static void main(String[] args) {
        Client client = new Main().new Client();
    }

    private class Client {
        private ClientService clientService = new ClientService();

        Client() {
            // Execute a sync every 10 seconds
            ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
            executor.scheduleAtFixedRate(this::sync, 0, 10, TimeUnit.SECONDS);
        }

        private void sync() {
            try {
                clientService.sync();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
