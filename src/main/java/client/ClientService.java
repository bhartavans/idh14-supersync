package client;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import protocol.*;
import protocol.models.CustomInputStream;
import protocol.models.FileEntry;
import protocol.models.Header;

import java.io.*;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Bram de Hart.
 */
class ClientService {

    private ProtocolService protocolService = new ProtocolService();
    private Socket socket;
    private InputStream inputStream;
    private OutputStream outputStream;
    private Header inputHeader;
    private Header outputHeader;
    private String inputBody;
    private byte[] outputBody;
    private String filePath = "src/main/java/client/files";
    private final File fileFolder = new File(filePath);
    private List<FileEntry> previousFiles;

    void openSocket() throws IOException {
        // Initialize socket and stream
        socket = new Socket(Config.Remote.HOST, Config.Remote.PORT);
        outputStream = socket.getOutputStream();
        inputStream = socket.getInputStream();
    }

    void closeSocket() throws IOException {
        // Close writer, stream and socket
        inputStream.close();
        outputStream.close();
        socket.close();
    }
    /**
     * Synchronize with the remote remote.
     */
    void sync() throws Exception {
        System.out.println("==================================================");
        System.out.println("Sync remote server");
        System.out.println("==================================================");

        // Retrieve the previous known client files and current client files
        previousFiles = listPreviousFiles();
        List<FileEntry> currentFiles = listCurrentFiles();

        // Compare the lists and handle necessary actions
        compareFiles(previousFiles, currentFiles);
    }

    /**
     * Get a list of the previous known client files.
     * The method converts the files json list into an readable object list.
     * @return
     */
    private List<FileEntry> listPreviousFiles() throws Exception {
        // Read the content of the json file
        StringBuilder json = new StringBuilder();
        FileInputStream fileInputStream = new FileInputStream("src/main/java/client/files/.files.json");;
        InputStreamReader inputStreamReader = new InputStreamReader(fileInputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);;
        String line;

        // Append each line to the json output
        while ((line = bufferedReader.readLine()) != null) {
            json.append(line);
        }

        // Close connections
        bufferedReader.close();
        inputStreamReader.close();
        fileInputStream.close();

        // Return a parsed json result as a list of file entries
        return (new Gson()).fromJson(json.toString(), new TypeToken<List<FileEntry>>() {
        }.getType());
    }

    /**
     * Get a list of the current client files.
     * This methods reads the current client files in the file directory and converts them into an readable object list.
     * @return
     */
    private List<FileEntry> listCurrentFiles() {
        List<FileEntry> fileEntries = new ArrayList<>();
        // Read files in file directory
        for (final File file : Objects.requireNonNull(fileFolder.listFiles())) {
            if (!file.getName().startsWith(".")) {
                FileEntry fileEntry = new FileEntry(file);
                // Get the previous checksum
                innerLoop:
                for (FileEntry prevFile: previousFiles) {
                    if (prevFile.fileName.equals(fileEntry.fileName)) {
                        fileEntry.originalCheckSum = prevFile.checksum;
                        break innerLoop;
                    }
                }
                fileEntries.add(fileEntry);
            }
        }
        return fileEntries;
    }

    /**
     * This method compares the retrieved lists to determine which files needs to be synced.
     * In other words: which files needs to be downloaded, uploaded or deleted.
     * @param prevClientFiles
     * @param clientFiles
     */
    private void compareFiles(List<FileEntry> prevClientFiles, List<FileEntry> clientFiles) throws Exception {
        /*
         * First, we check if there have been deleted files on the client.
         * If so, we make sure to delete them on the remote remote.
         */
        List<FileEntry> deletedClientFiles = new ArrayList<>();
        for (FileEntry prevClientFile: prevClientFiles) {
            boolean isDeleted = true;
            for (FileEntry clientFile: clientFiles) {
                if (clientFile.fileName.equals(prevClientFile.fileName)) {
                    isDeleted = false;
                }
            }
            if (isDeleted) {
                deletedClientFiles.add(prevClientFile);
            }
        }

        // Delete on the remote server
        for (FileEntry deletedClientFile: deletedClientFiles) {
            try {
                deleteRemote(deletedClientFile.fileName);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
         * Second, we check if there have been updated or new files on the client side.
         * If so, we make sure to upload them on the remote.
         */
        List<FileEntry> changedClientFiles = new ArrayList<>();
        for (FileEntry clientFile: clientFiles) {
            boolean isNew = true;
            for (FileEntry prevClientFile: prevClientFiles) {
                if (clientFile.fileName.equals(prevClientFile.fileName)) {
                    isNew = false;
                    if (!clientFile.checksum.equals(clientFile.originalCheckSum)) {
                        // The client file has been changed
                        System.out.println("Checksums do not match for file: " + clientFile.fileName);
                        System.out.println("Checksum current client: " + clientFile.checksum);
                        System.out.println("Checksum previous client: " + prevClientFile.checksum);
                        changedClientFiles.add(clientFile);
                    }
                }
            }
            if (isNew) {
                changedClientFiles.add(clientFile);
            }
        }

        // Upload on the remote server
        for (FileEntry changedClientFile: changedClientFiles) {
            try {
                upload(changedClientFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
         * Third, we retrieve the remote files and check if there have been files added or modified.
         * If so, we add or edit them on the client side as well.
         */
        List<FileEntry> remoteFiles = this.listRemoteFiles();

        List<FileEntry> changedRemoteFiles = new ArrayList<>();
        for (FileEntry fileEntry: remoteFiles) {
            boolean isNew = true;
            for (FileEntry clientFile: clientFiles) {
                if (fileEntry.fileName.equals(clientFile.fileName)) {
                    isNew = false;
                    if (!fileEntry.checksum.equals(clientFile.checksum)) {
                        // The remote file has been changed
                        System.out.println("Checksums do not match for file: " + clientFile.fileName);
                        System.out.println("Checksum client: " + clientFile.checksum);
                        System.out.println("Checksum remote: " + fileEntry.checksum);
                        changedRemoteFiles.add(fileEntry);
                    }

                    // For file conflict comparison
                    fileEntry.originalCheckSum = clientFile.originalCheckSum;
                    fileEntry.checksum = clientFile.checksum;
                }
            }
            if (isNew) {
                changedRemoteFiles.add(fileEntry);
            }
        }
        // Download from the remote
        for (FileEntry changedServerFile: changedRemoteFiles) {
            try {
                download(changedServerFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        /*
         * Last, we check if there have been files deleted on the remote.
         * If so, we delete them on the client as well.
         */
        for (FileEntry clientFile: clientFiles) {
            boolean isDeleted = true;
            for (FileEntry remoteFile: remoteFiles) {
                if (remoteFile.fileName.equals(clientFile.fileName)) {
                    isDeleted = false;
                }
            }
            if (isDeleted) {
                deleteClient(clientFile.fileName);
            }
        }

        // Update the JSON list for future comparisons
        updateJson();
    }

    /**
     * List files from the remote server.
     */
    private List<FileEntry> listRemoteFiles() throws IOException {
        System.out.println("==================================================");
        System.out.println("List remote files");
        System.out.println("==================================================");

        // Open TCP socket connection
        openSocket();

        // Define output header and sent request
        outputHeader = new Header();
        outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
        outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
        outputHeader.method = Config.Header.Methods.LIST;
        outputStream.write(protocolService.encodeHeader(outputHeader));

        // Read input header
        CustomInputStream customInputStream = new CustomInputStream(inputStream);
        ArrayList<String> inputHeaders = new ArrayList<>();
        String line = customInputStream.readLine();
        while (line != null && !line.isEmpty()) {
            inputHeaders.add(line);
            line = customInputStream.readLine();
        }
        inputHeader = protocolService.decodeHeader(inputHeaders);
        if (inputHeader.responseCode == Config.Header.Responses.OK.getCode()) {

            // Read input body
            long size = inputHeader.contentSize;
            long consumed = 0;
            StringBuilder body = new StringBuilder();

            byte[] chunk = new byte[1024]; // Chunks of 1kb for efficiency
            while (consumed < size) {
                int chunkSize = (int) Math.min(chunk.length, size - consumed);
                int read = customInputStream.read(chunk, 0, chunkSize);
                body.append(new String(chunk, 0, read, StandardCharsets.UTF_8));  // Because of JSON response
                consumed += read;
            }

            inputBody = body.toString();
        }

        // Close TCP socket connection
        closeSocket();

        // Convert json result to file list and return
        return (new Gson()).fromJson(inputBody, new TypeToken<List<FileEntry>>() {
        }.getType());
    }

    /**
     * Delete file from the remote remote.
     * @param fileName
     */
    private void deleteRemote(String fileName) throws IOException {
        System.out.println("==================================================");
        System.out.println("Delete remote file: " + fileName);
        System.out.println("==================================================");

        // Open TCP socket connection
        openSocket();

        // Define output header and sent request
        outputHeader = new Header();
        outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
        outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
        outputHeader.method = Config.Header.Methods.DELETE;
        outputHeader.contentName = fileName;
        outputStream.write(protocolService.encodeHeader(outputHeader));

        // Read input header
        CustomInputStream customInputStream = new CustomInputStream(inputStream);
        ArrayList<String> inputHeaders = new ArrayList<>();
        String line = customInputStream.readLine();
        while (line != null && !line.isEmpty()) {
            inputHeaders.add(line);
            line = customInputStream.readLine();
        }
        inputHeader = protocolService.decodeHeader(inputHeaders);

        // Close TCP socket connection
        closeSocket();
    }

    /**
     * Delete file from the client.
     * @param fileName
     */
    private void deleteClient(String fileName) throws IOException {
        System.out.println("==================================================");
        System.out.println("Delete client file: " + fileName);
        System.out.println("==================================================");

        // Read file in file directory
        File file = new File(filePath + "/" + fileName);
        if (!file.exists()) {
            // File not found. Assign output headers.
            outputHeader = new Header();
            outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
            outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
            outputHeader.responseCode = Config.Header.Responses.NOT_FOUND.getCode();
            outputHeader.responseName = Config.Header.Responses.NOT_FOUND.getName();
            outputHeader.contentName = fileName;
            // Sent response
            outputStream.write(protocolService.encodeHeader(outputHeader));
        } else {
            // File found
            Files.delete(file.toPath());
        }
    }

    /**
     * Download file from the remote remote.
     * @param fileEntry
     */
    private void download(FileEntry fileEntry) throws IOException {
        System.out.println("==================================================");
        System.out.println("Download file: " + fileEntry.fileName);
        System.out.println("==================================================");

        // Open TCP socket connection
        openSocket();

        // Check if there is an file conflict
        if (fileEntry.originalCheckSum != null && !fileEntry.originalCheckSum.equals(fileEntry.checksum)) {
            outputHeader = new Header();
            outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
            outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
            outputHeader.responseCode = Config.Header.Responses.FILE_CONFLICT.getCode();
            outputHeader.responseName = Config.Header.Responses.FILE_CONFLICT.getName();
            outputHeader.contentName = fileEntry.fileName;

            // Write header
            outputStream.write(outputHeader.toByteArray());

            // Sent request
            outputStream.flush();
            return;
        }

        // Define output header and sent request
        outputHeader = new Header();
        outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
        outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
        outputHeader.method = Config.Header.Methods.DOWNLOAD;
        outputHeader.contentName = fileEntry.fileName;
        outputStream.write(protocolService.encodeHeader(outputHeader));

        // Read input header
        CustomInputStream customInputStream = new CustomInputStream(inputStream);
        ArrayList<String> inputHeaders = new ArrayList<>();
        String line = customInputStream.readLine();
        while (line != null && !line.isEmpty()) {
            inputHeaders.add(line);
            line = customInputStream.readLine();
        }
        inputHeader = protocolService.decodeHeader(inputHeaders);
        if (inputHeader.responseCode == Config.Header.Responses.OK.getCode()) {

            File file = new File(filePath + "/" + inputHeader.contentName);
            BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));

            // If file does not exist, create it
            if (!file.exists()) {
                file.createNewFile();
            }

            // Read input body
            long size = inputHeader.contentSize;
            long consumed = 0;
            byte[] buffer = new byte[(int) size];
            byte[] chunk = new byte[1024]; // Chunks of 1kb for efficiency
            while (consumed < size) {
                int chunkSize = (int) Math.min(chunk.length, size - consumed);
                int read = customInputStream.read(buffer, 0, chunkSize);
                bufferedOutputStream.write(buffer, 0, chunkSize);
                consumed += read;
            }

            // Close buffer stream
            bufferedOutputStream.close();

            // Check if checksums do match
            FileEntry downloadedFileEntry = new FileEntry(file);
            System.out.println("Checksum header: " + inputHeader.contentCheckSum);
            System.out.println("Checksum downloaded file: " + downloadedFileEntry.checksum);

            // Define response header
            outputHeader = new Header();
            outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
            outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
            outputHeader.responseCode = Config.Header.Responses.OK.getCode();
            outputHeader.responseName = Config.Header.Responses.OK.getName();
            if (!inputHeader.contentCheckSum.equals(downloadedFileEntry.checksum)) {
                System.out.println("Checksums do not match for file: " + downloadedFileEntry.fileName);
                outputHeader.responseCode = Config.Header.Responses.CHECKSUM_MISMATCH.getCode();
                outputHeader.responseName = Config.Header.Responses.CHECKSUM_MISMATCH.getName();
            }

            // Write header
            outputStream.write(outputHeader.toByteArray());

            // Sent request
            outputStream.flush();
        }

        // Close TCP socket connection
        closeSocket();
    }

    /**
     * Upload file from the client.
     * @param fileEntry
     */
    private void upload(FileEntry fileEntry) throws IOException {
        System.out.println("==================================================");
        System.out.println("Upload file: " + fileEntry.fileName);
        System.out.println("==================================================");

        // Open TCP socket connection
        openSocket();

        // Define output header and sent request
        outputHeader = new Header();
        outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
        outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
        outputHeader.method = Config.Header.Methods.UPLOAD;
        outputHeader.contentName = fileEntry.fileName;
        outputHeader.contentSize = fileEntry.contentSize;
        outputHeader.contentCheckSum = fileEntry.checksum;
        outputHeader.contentOriginalChecksum = fileEntry.originalCheckSum;
        outputStream.write(protocolService.encodeHeader(outputHeader));

        // Read file in file directory
        File file = new File(filePath + "/" + fileEntry.fileName);
        // Assign bytes to output body
        long size = fileEntry.contentSize;
        outputBody = new byte[(int) size];
        BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
        bufferedInputStream.read(outputBody, 0, outputBody.length);
        bufferedInputStream.close();

        // Write header and body
        outputStream.write(outputHeader.toByteArray());
        outputStream.write(outputBody);
        outputStream.flush();

        // Read input header
        CustomInputStream customInputStream = new CustomInputStream(inputStream);
        ArrayList<String> inputHeaders = new ArrayList<>();
        String line = customInputStream.readLine();
        while (line != null && !line.isEmpty()) {
            inputHeaders.add(line);
            line = customInputStream.readLine();
        }
        inputHeader = protocolService.decodeHeader(inputHeaders);

        // Close TCP socket connection
        closeSocket();
    }

    /**
     * Update JSON file list from the client.
     */
    private void updateJson() throws IOException {
        System.out.println("==================================================");
        System.out.println("Update JSON list");
        System.out.println("==================================================");

        List<FileEntry> fileEntries = new ArrayList<>();
        String jsonFileList;

        // Read files in file directory
        for (final File file : Objects.requireNonNull(fileFolder.listFiles())) {
            if (!file.getName().startsWith(".")) {
                fileEntries.add(new FileEntry(file));
            }
        }

        // Convert file list to JSON object
        jsonFileList = (new Gson()).toJson(fileEntries);

        // Write into json file
        File file = new File(filePath + "/" + ".files.json");
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));
        bufferedOutputStream.write(jsonFileList.getBytes(), 0, jsonFileList.length());

        // Close buffer stream
        bufferedOutputStream.close();
    }

}
