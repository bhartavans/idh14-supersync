package remote;

/**
 * Created by Bram de Hart.
 */
public class Main {
    private RemoteService syncService;

    public static void main(String[] args) {
        Server server = new Main().new Server();
    }

    private class Server {

        /**
         * Constructor. Initialize a new sync service.
         */
        Server() {
            syncService = new RemoteService();
        }
    }

}
