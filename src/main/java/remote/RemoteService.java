package remote;

import com.google.gson.Gson;
import protocol.*;
import protocol.Config.*;
import protocol.models.CustomInputStream;
import protocol.models.FileEntry;
import protocol.models.Header;
import protocol.models.Response;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by Bram de Hart.
 */
class RemoteService {
    private ProtocolService protocolService = new ProtocolService();
    private InputStream inputStream;
    private OutputStream outputStream;
    private CustomInputStream customInputStream;
    private Header inputHeader;
    private Header outputHeader;
    private byte[] inputBody;
    private byte[] outputBody;
    private String filePath = "src/main/java/remote/files";
    private final File fileFolder = new File(filePath);

    /**
     * Constructor. Initialize a new socket.
     */
    RemoteService() {
        try {
            // Initialize a socket so the remote can listen to incoming requests.
            ServerSocket serverSocket = new ServerSocket(Remote.PORT);

            // Start listening to incoming requests
            while (true) {
                Socket socket = serverSocket.accept();
                inputStream = socket.getInputStream();
                outputStream = socket.getOutputStream();

                // Read input header
                customInputStream = new CustomInputStream(inputStream);
                ArrayList<String> inputHeaders = new ArrayList<>();
                String line = customInputStream.readLine();
                while (line != null && !line.isEmpty()) {
                    inputHeaders.add(line);
                    line = customInputStream.readLine();
                }
                inputHeader = protocolService.decodeHeader(inputHeaders);

                // Switch case request method
                String method = inputHeader.method;
                if (method != null) {
                    switch (method) {
                        case Config.Header.Methods.LIST:
                            listFiles();
                            break;
                        case Config.Header.Methods.DOWNLOAD:
                            downloadFile();
                            break;
                        case Config.Header.Methods.UPLOAD:
                            uploadFile();
                            break;
                        case Config.Header.Methods.DELETE:
                            deleteFile();
                            break;
                }

                }

            }
        } catch (IOException e) {
            System.out.println("Unable to initialize the remote socket.");
            e.printStackTrace();
        }
    }

    private void listFiles() {
        System.out.println("==================================================");
        System.out.println("List files");
        System.out.println("==================================================");

        List<FileEntry> fileEntries = new ArrayList<>();
        String jsonFileList = "";
        Response response = Config.Header.Responses.OK;
        outputHeader = new Header();
        outputBody = new byte[]{};

        try {
            // Read files in file directory
            for (final File file : Objects.requireNonNull(fileFolder.listFiles())) {
                if (!file.getName().startsWith(".")) {
                    fileEntries.add(new FileEntry(file));
                }
            }
            // Convert file list to JSON object
            jsonFileList = (new Gson()).toJson(fileEntries);
        } catch (Exception exception) {
            response = Config.Header.Responses.SERVER_ERROR;
        } finally {
            try {
                // Define output header
                outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
                outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
                outputHeader.responseCode = response.getCode();
                outputHeader.responseName = response.getName();

                // Define output body
                if (response.getCode() == Config.Header.Responses.OK.getCode()) {
                    outputHeader.contentSize = (long) jsonFileList.length();
                    outputBody = (jsonFileList).getBytes();
                }

                // Write to output stream
                outputStream.write(outputHeader.toByteArray());
                outputStream.write(outputBody);

                // Sent request
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void downloadFile() throws IOException {
        System.out.println("==================================================");
        System.out.println("Download file: " + inputHeader.contentName);
        System.out.println("==================================================");

        outputHeader = new Header();
        outputBody = new byte[]{};

        // Define default output header values
        outputHeader.responseCode = Config.Header.Responses.OK.getCode();
        outputHeader.responseName = Config.Header.Responses.OK.getName();
        outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
        outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;

        try {
            // Read file in file directory
            File file = new File(filePath + "/" + inputHeader.contentName);
            if (!file.exists()) {
                // File not found. Assign output headers.
                outputHeader.responseCode = Config.Header.Responses.NOT_FOUND.getCode();
                outputHeader.responseName = Config.Header.Responses.NOT_FOUND.getName();
            } else {
                // File found
                // Assign output header properties
                FileEntry fileEntry = new FileEntry(file);
                outputHeader.contentName = fileEntry.fileName;
                outputHeader.contentSize = fileEntry.contentSize;
                outputHeader.contentCheckSum = fileEntry.checksum;
                // Assign bytes in body
                final byte[] fileBytes = new byte[(int) file.length()];
                BufferedInputStream bufferedInputStream = new BufferedInputStream(new FileInputStream(file));
                bufferedInputStream.read(fileBytes, 0, fileBytes.length);
                outputBody = fileBytes;
            }
        } catch (Exception e) {
            // Exception (Server Error). Assign output headers.
            outputHeader.responseCode = Config.Header.Responses.SERVER_ERROR.getCode();
            outputHeader.responseName = Config.Header.Responses.SERVER_ERROR.getName();
        }

        // Write header and body
        outputStream.write(outputHeader.toByteArray());
        if (outputBody.length > 0) {
            outputStream.write(outputBody);
        }

        // Sent request
        outputStream.flush();

        // Read input header
        CustomInputStream customInputStream = new CustomInputStream(inputStream);
        ArrayList<String> inputHeaders = new ArrayList<>();
        String line = customInputStream.readLine();
        while (line != null && !line.isEmpty()) {
            inputHeaders.add(line);
            line = customInputStream.readLine();
        }
        inputHeader = protocolService.decodeHeader(inputHeaders);
    }

    private void uploadFile() throws IOException {
        System.out.println("==================================================");
        System.out.println("Upload file: " + inputHeader.contentName);
        System.out.println("==================================================");

        CustomInputStream customInputStream = new CustomInputStream(inputStream);

        // Skip headers
        String line = customInputStream.readLine();
        while (line != null && !line.isEmpty()) {
            line = customInputStream.readLine();
        }

        // Assign buffered output stream to read body
        File file = new File(filePath + "/" + inputHeader.contentName);

        // If file does not exist, create it
        if (!file.exists()) {
            file.createNewFile();
        }

        FileEntry fileEntry = new FileEntry(file);

        // Check if there is an file conflict
        if (inputHeader.contentOriginalChecksum != null && !inputHeader.contentOriginalChecksum.equals(fileEntry.checksum)) {
            outputHeader = new Header();
            outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
            outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
            outputHeader.responseCode = Config.Header.Responses.FILE_CONFLICT.getCode();
            outputHeader.responseName = Config.Header.Responses.FILE_CONFLICT.getName();
            outputHeader.contentName = fileEntry.fileName;

            // Write header
            outputStream.write(outputHeader.toByteArray());

            // Sent request
            outputStream.flush();
            return;
        }

        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(file));

        // Read input body
        long size = inputHeader.contentSize;
        long consumed = 0;
        byte[] buffer = new byte[(int) size];
        byte[] chunk = new byte[1024]; // Chunks of 1kb for efficiency
        while (consumed < size) {
            int chunkSize = (int) Math.min(chunk.length, size - consumed);
            int read = customInputStream.read(buffer, 0, chunkSize);
            bufferedOutputStream.write(buffer, 0, chunkSize);
            consumed += read;
        }

        // Close streams
        bufferedOutputStream.close();
        outputStream.flush();

        // Define response header
        outputHeader = new Header();
        outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
        outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;
        outputHeader.responseCode = Config.Header.Responses.OK.getCode();
        outputHeader.responseName = Config.Header.Responses.OK.getName();
        if (!inputHeader.contentCheckSum.equals(fileEntry.checksum)) {
            System.out.println("Checksums do not match for file: " + fileEntry.checksum);
            outputHeader.responseCode = Config.Header.Responses.CHECKSUM_MISMATCH.getCode();
            outputHeader.responseName = Config.Header.Responses.CHECKSUM_MISMATCH.getName();
        }

        // Write header
        outputStream.write(outputHeader.toByteArray());

        // Sent request
        outputStream.flush();
    }

    private void deleteFile() throws IOException {
        System.out.println("==================================================");
        System.out.println("Delete file: " + inputHeader.contentName);
        System.out.println("==================================================");

        outputHeader = new Header();

        // Define default output header values
        outputHeader.responseCode = Config.Header.Responses.OK.getCode();
        outputHeader.responseName = Config.Header.Responses.OK.getName();
        outputHeader.protocolName = Config.Header.PROTOCOL_NAME;
        outputHeader.protocolVersion = Config.Header.PROTOCOL_VERSION;

        try {
            // Read file in file directory
            File file = new File(filePath + "/" + inputHeader.contentName);
            if (!file.exists()) {
                // File not found. Assign output headers.
                outputHeader.responseCode = Config.Header.Responses.NOT_FOUND.getCode();
                outputHeader.responseName = Config.Header.Responses.NOT_FOUND.getName();
            } else {
                // File found
                Files.delete(file.toPath());

            }
        } catch (Exception e) {
            // Exception (Server Error). Assign output headers.
            outputHeader.responseCode = Config.Header.Responses.SERVER_ERROR.getCode();
            outputHeader.responseName = Config.Header.Responses.SERVER_ERROR.getName();
        }

        // Write header
        outputStream.write(outputHeader.toByteArray());

        // Sent request
        outputStream.flush();
    }
}
